var express = require('express');   //referencia al paquete express
var app = express();

//operación GET del 'Hola Mundo'
app.get('/holamundo',
    function(request, response) {
      response.send('Hola Perú!');
});

//Servidor escuchará en la URL (servidor local)
//Http://localhost:3000/holamundo
app.listen(3000, function(){
  console.log('Node JS escuchando en el pueto 3000...');
});
